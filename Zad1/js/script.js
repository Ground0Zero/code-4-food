function addElements() {
	var colors = ["green", "blue", "#b0b0b0", "#ff0000", "navy", "#a0b1c3", "#555", "#fff", "yellow", "#f5a3b5"];
	var createdDivsSection = document.getElementById('Divs');

	var divs = document.getElementById('theValue').value;

	for(var i = 0; i < divs; i++) {
		var newdiv = document.createElement('div');

		newdiv.style.top = i*10 + 'px';
		newdiv.style.left = i*10 + 'px';

		newdiv.style.backgroundColor = colors[Math.floor(Math.random() * 10)];
		createdDivsSection.appendChild(newdiv);
	}
}

